﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct playerObject
{
    GameObject playerGameObject;
    Camera playerCameraObject;
    PlayerProperty playerProperty;

    public playerObject(GameObject playerObject)
    {
        playerGameObject = playerObject;
        playerCameraObject = playerObject.GetComponentInChildren<Camera>();
        playerProperty = playerObject.GetComponentInChildren<PlayerProperty>();
    }

    public GameObject GetPlayerObject()
    {
        return playerGameObject;
    }
    
    public Camera GetPlayerCamera()
    {
        return playerCameraObject;
    }

    public PlayerProperty GetPlayerProperty()
    {
        return playerProperty;
    }
}

public class GameMode : MonoBehaviour
{
    private int ammountOfBall = 0;
    public GameObject playerPrefab;
    public GameObject ballPrefab;
    public int ballAmmount2Player = 0;
    public int ballAmmount3Player = 0;
    public int ballAmmount4Player = 0;

    List<playerVariable> playersInfo;
    List<playerObject> playersInScene;
    List<playerObject> playersAlive;
    public Transform[] playerSpawnPoint;
    public Transform[] ballSpawnPoint;
    bool[] playerSpawnTaken;
    bool[] ballSpawnTaken;

    private SoundManager SM;

    bool musicOn;

    private void Awake()
    {
        //Instantiate variable so it would work
        playersInfo = new List<playerVariable>();
        playersInScene = new List<playerObject>();

        //Get Player Info if there is in GameStaticVaraible
        if (GameStaticVariable.isTherePlayer())
        {
            playersInfo = GameStaticVariable.GetPlayer();
            if (playersInfo.Count > playerSpawnPoint.Length)
            {
                Debug.Log("There are less spawn point then the ammount of player");
                return;
            }
            else if (playersInfo.Count == 1)
            {
                Debug.Log("Whats the point of playing?");
                return;
            }
            else if (playersInfo.Count > 4)
            {
                Debug.Log("Too much player Dont know what to do");
                return;
            }
        }
        else
        {
            Debug.LogError("There is no Player");
            return;
        }

        //Spawnning in Player
        playerSpawnTaken = new bool[playerSpawnPoint.Length];
        ballSpawnTaken = new bool[ballSpawnPoint.Length];

        foreach (playerVariable p in playersInfo)
        {
            bool playerSpawn = false;
            while (!playerSpawn)
            {

                int rand = Random.Range(0, playerSpawnPoint.Length);
                if (!playerSpawnTaken[rand])
                {
                    GameObject player = Instantiate(playerPrefab, playerSpawnPoint[rand].position, playerSpawnPoint[rand].rotation);
                    Destroy(player.GetComponentInChildren<AudioListener>());
                    PlayerProperty PP = player.GetComponentInChildren<PlayerProperty>();
                    PP.setPlayer(p.getPlayerColor(), p.getPlayerID(), p.getJoyID());
                    Camera playerCamera = player.GetComponentInChildren<Camera>();
                    playersInScene.Add( new playerObject(player));
                    playerSpawn = true;
                    playerSpawnTaken[rand] = true;
                }
            }
        }

        //Setting up Camera
        if (playersInScene.Count == 2)
        {
            ammountOfBall = ballAmmount2Player;
            playersInScene[0].GetPlayerCamera().rect = new Rect(0.0f, 0.0f, 0.5f, 1.0f);
            playersInScene[1].GetPlayerCamera().rect = new Rect(0.5f, 0.0f, 0.5f, 1.0f);
        }
        else if (playersInScene.Count > 2)
        {
            ammountOfBall = ballAmmount3Player;
            playersInScene[0].GetPlayerCamera().rect = new Rect(0.0f, 0.5f, 0.5f, 0.5f);
            playersInScene[1].GetPlayerCamera().rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
            playersInScene[2].GetPlayerCamera().rect = new Rect(0.0f, 0.0f, 0.5f, 0.5f);
            if (playersInScene.Count >= 4)
            {
                ammountOfBall = ballAmmount4Player;
                playersInScene[3].GetPlayerCamera().rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
            }
        }

        playersAlive = playersInScene;
        if(ammountOfBall == 0)
        {
            ammountOfBall = playersInScene.Count;
        } 
        if (ammountOfBall > ballSpawnPoint.Length)
        {
            Debug.LogError("To many balls not enough spawn point to spawn them");
        }
        else {
            for (int i = 0; i < ammountOfBall; i++)
            {
                bool ballSpawn = false;
                while (!ballSpawn)
                {
                    int rand = Random.Range(0, ballSpawnPoint.Length);
                    if (!ballSpawnTaken[rand])
                    {
                        Instantiate(ballPrefab, ballSpawnPoint[rand].position, ballSpawnPoint[rand].rotation);
                        ballSpawn = true;
                        ballSpawnTaken[rand] = true;
                    }
                }
            }
        }
        SM = FindObjectOfType<SoundManager>();
        //Setting up Death player array
    }
    // Start is called before the first frame update
    void Start()
    {

        SM.PlayMusic("InGameMusic1");
        musicOn = true;
     }


    // Update is called once per frame
    void Update()
    {
        if (Pause.isPaused && musicOn)
        {
            musicOn = false;
            SM.PauseMusic("InGameMusic1");
        }
        else if (!musicOn && !Pause.isPaused)
        {
            SM.PlayMusic("InGameMusic1");
            musicOn = true;
        }
        if (playersAlive.Count > 1)
        {
            int i = 0;
            List<int> playerDead = new List<int>();
            foreach (playerObject p in playersAlive)
            {
                PlayerProperty PP = p.GetPlayerProperty();
                if (PP != null)
                {
                    if (PP.getIsDead())
                    {
                        Debug.Log("Dead");
                        playerDead.Add(i);
                    }
                }
                i++;
            }
            foreach(int o in playerDead)
            {
                playersAlive.RemoveAt(o);
            }
        }
        //else if (playersAlive.Count == 1)
        //{
        //    //Debug.Log("Win");
        //}
  
 
    }

    public List<playerObject> GetAlivePlayers() {
        return playersAlive;
    }

    public int PlayerIDWon()
    {
        if(playersAlive.Count == 1)
        {
            return playersAlive[0].GetPlayerProperty().getPlayerID();
        }
        return -1;
    }
    public int playerAliveCount()
    {
        return playersAlive.Count;
    }
}
