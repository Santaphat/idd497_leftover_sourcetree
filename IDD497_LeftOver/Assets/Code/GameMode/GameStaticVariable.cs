﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct playerVariable{
    private int playerID;
    private int joyID;
    private Color playerColor;
    public playerVariable(Color playerColor, int playerID,  int joyID)
    {
        this.playerID = playerID;
        this.joyID = joyID;
        this.playerColor = playerColor;
    }

    public int getPlayerID()
    {
        return playerID;
    }

    public int getJoyID()
    {
        return joyID;
    }

    public Color getPlayerColor()
    {
        return playerColor;
    }
}
public static class GameStaticVariable
{
    static List<playerVariable> players;

    public static void setPlayer( List<playerVariable> player)
    {
        players = player;
    }

    public static bool isTherePlayer()
    {
        if(players == null)
        {
            return false;
        }
        return true;
    }

    public static List<playerVariable> GetPlayer()
    {
        return players;
    }


}
