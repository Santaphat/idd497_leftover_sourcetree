﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Transform modelAnimation;
    public Animator ac;
    private PlayerProperty PP;
    // Start is called before the first frame update
    void Start()
    {
        PP = gameObject.GetComponent<PlayerProperty>();
        ac = PP.AC;
        if (!ac)
        {
            Debug.Log("Controller Not Found");
        }
    }

    // Update is called once per frame
    void Update()
    {
        ac.SetFloat("inputH", PP.getX());
        ac.SetFloat("inputV", PP.getZ());
        ac.SetBool("isSprinting", PP.getIsSprinting());
        ac.SetBool("haveBall", PP.getHaveBall());
        ac.SetBool("isCrouching", PP.getIsCrouching());
        ac.SetBool("startJumping", PP.jumpStart);
        ac.SetBool("isFalling", PP.isFalling);
        ac.SetBool("landed", PP.landed);
        ac.SetBool("isCharging", PP.getIsCharging());
        ac.SetBool("isThrowing", PP.getIsThrowing());
        if (ac.GetCurrentAnimatorStateInfo(0).IsName("Landed") && ac.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            PP.landed = false;
        }
        if(ac.GetCurrentAnimatorStateInfo(1).IsName("Throw") && ac.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            PP.SetIsThrowingToFalse();
        }
    }

}
