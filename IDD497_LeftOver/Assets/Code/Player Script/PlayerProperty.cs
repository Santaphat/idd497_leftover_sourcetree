﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerProperty : MonoBehaviour
{

    //Public Variable
    public Transform body;
    public Transform orientation;
    public Transform Hand;
    public Transform cameraTransform;
    public Transform model;
    public Transform surfaceModel;
    public Transform throwPosition;

    private SoundManager SM;

    //public Camera camera;


    //PlyerMovement Variable
    public float groundDistance = 0.5f;
    public float characterSize = 2.0f;

    public float walkAcc = 4500.0f;
    public float walkMaxSpeed = 15.0f;

    public float runAcc = 5500.0f;
    public float runMaxSpeed = 20.0f;

    public float runStaminaTime = 3.0f;

    private float currentStaminaTime = 0.0f;

    public float jumpForce = 550.0f;

    public float thresHold = 0.01f;
    public float counterMovement = 0.175f;

    //Throwing Variable

    public float throwForceMax = 100.0f;
    public float throwForceMin = 20.0f;
    public float throwChargeTime = 10.0f;
    //public float trapThrowForce = 20.0f;

    //public GameObject trapPrefab;

    //public GameObject CaptureBallTriggerBox;

    //Camera Variable
    public float mouseSensitivity = 100.0f;
    public float controllerSensitivity = 50.0f;

    //Input Variable
    public float radiusPickingUpBall = 0.1f;
    public float pickUpBallDis = 1.3f;

    //Private Variable
    //Player Info
    [HideInInspector]
    public Rigidbody RB;
    [HideInInspector]
    public Animator AC;
    [HideInInspector]
    public CapsuleCollider RC;
    [HideInInspector]
    public Collider[] ragDollCollider;
    [HideInInspector]
    public Rigidbody[] ragDollRigidBody;
    public float normalToCrouchRadiusPlus = 0.5f;
    public float normalToCrouchHeightMinus = 0.8f;

    [HideInInspector]
    public int playerID;
    [HideInInspector]
    public int joyID;
    private int offSetToNextJoy = 20;
    [HideInInspector]
    public Color playerColor = new Color(0,0,0);
    private SkinnedMeshRenderer RD;

    //PlayerMovement
    private float x;
    private float z;
    private bool isSprinting;
    private bool isDead;
    [HideInInspector]
    public bool jumpStart;
    [HideInInspector]
    public bool isFalling;
    [HideInInspector]
    public bool landed;
    [HideInInspector]
    public bool isCatching;
    private PlayerMovement PM;
    private bool stoodUp;

    //Action
    //Ball Throwing
    private float currentThrowForce;
    private bool haveBall;
    private bool isCharging;
    private bool isThrowing;
    private bool catchingBall;
    private bool isCrouching;
    private Collider ballColliderObj;
    private Rigidbody ballRigidObj;
    //Ball Catching
    private CaptureBallTrigger capTrigger;

    // Start is called before the first frame update

    public PlayerProperty()
    {
        this.playerID = 1;
        this.joyID = 0;
        this.playerColor = new Color(1, 0, 0);
    }
    public void setPlayer(Color playerColor ,int playerID, int joyNumber = 0)
    {
        this.playerColor = playerColor;
        this.playerID = playerID;
        this.joyID = joyNumber;
    }

    public void getComponentVariable()
    {
        SM = FindObjectOfType<SoundManager>(); 
        RB = gameObject.GetComponent<Rigidbody>();
        AC = model.GetComponent<Animator>();
        RC = gameObject.GetComponent<CapsuleCollider>();
        RD = surfaceModel.GetComponent<SkinnedMeshRenderer>();
        PM = gameObject.GetComponent<PlayerMovement>();

        ragDollCollider = model.GetComponentsInChildren<Collider>();
        ragDollRigidBody = model.GetComponentsInChildren<Rigidbody>();
    }

    void RagdollActive(bool active)
    {
        foreach(Collider c in ragDollCollider)
        {
            c.enabled = active;
        }
        foreach(Rigidbody r in ragDollRigidBody)
        {
            r.detectCollisions = active;
            r.isKinematic = !active;
        }

        AC.enabled = !active;
        RB.detectCollisions = !active;
        RB.isKinematic = active;
        RC.enabled = !active;
    }

    private void Awake()
    {
        getComponentVariable();
        RagdollActive(false);
    }
    void Start()
    {
        currentStaminaTime = runStaminaTime;
        stoodUp = true;
        isDead = false;
        gameObject.tag = "Player " + playerID;
        Cursor.lockState = CursorLockMode.Locked;
        haveBall = false;
        RD.material.color = playerColor;
        RD.material.SetColor("_EmissionColor", playerColor);
        //int p = 1;
        //string[] names = Input.GetJoystickNames();
        //foreach (string o in names)
        //{
        //    Debug.Log(p + o);
        //    p++;
        //}
        //Debug.Log(KeyCode.Joystick1Button0 + 20);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead && !Pause.isPaused)
        {
            UpdatePlayer();
            //Debug.DrawRay(cameraTransform.position, cameraTransform.forward * pickUpBallDis);
            Look();
            if (joyID == 0)
            {
                MyPCInput();

            }
            else
            {
                controllerInput();
            }

           
        }
    }

    private float xRotation;
    private float desiredX;
    private void Look()
    {
        float rawX = 0;
        float rawY = 0;
        if (joyID == 0)
        {
            rawX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.fixedDeltaTime;
            rawY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.fixedDeltaTime;
        }
        else
        {
            rawX = Input.GetAxis("Joy" + joyID + "ThumbStickRightHorizontal");
            rawY = Input.GetAxis("Joy" + joyID + "ThumbStickRightVertical");
            if (Mathf.Abs(rawX) < 0.2f)
            {
                rawX = 0;
            }
            if (Mathf.Abs(rawY) < 0.2f)
            {
                rawY = 0;
            }
            rawX = rawX * controllerSensitivity * Time.fixedDeltaTime;
            rawY = rawY * controllerSensitivity * Time.fixedDeltaTime;
        }
        //Find current look rotation
        Vector3 rot = cameraTransform.transform.localRotation.eulerAngles;
        desiredX = rot.y + rawX;

        //Rotate, and also make sure we dont over- or under-rotate.
        xRotation -= rawY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        //Perform the rotations
        cameraTransform.transform.localRotation = Quaternion.Euler(xRotation, desiredX, 0);
        orientation.transform.localRotation = Quaternion.Euler(0, desiredX, 0);
    }

    void UpdatePlayer()
    {
        //Controlling animation and other stuff
        if (getIsCrouching() && stoodUp)
        {
            PM.StartCrouch();
            stoodUp = false;
        }
        else if (!getIsCrouching() && !stoodUp)
        {
            stoodUp = true;
            PM.StopCrouch();
        }
        if (jumpStart)
        {
            PM.StartJump();
        }

        bool isGrounded = PM.IsGrounded();
        if (!isGrounded)
        {
            isFalling = true;
        }
        else if (isGrounded && isFalling)
        {
            PM.landed();
        }
        if (isSprinting)
        {
            if (x < 0.1f && x > -0.1f)
            {
                if (z < 0.1f && z > -0.1f)
                {
                    StopSprinting();
                }
            }
            if (currentStaminaTime <= 0)
            {
                StopSprinting();
            }
            else
            {
                currentStaminaTime -= Time.deltaTime;
            }
        }
        else
        {
            if(currentStaminaTime <= runStaminaTime)
            {
                currentStaminaTime += Time.deltaTime;
            }
        }
    }

    void StartSprinting()
    {
        if (currentStaminaTime / runStaminaTime > 0.8f)
        {
            if (isCrouching)
            {
                isCrouching = false;
            }
            if (x > 0.1f || x < -0.1f)
            {
                isSprinting = true;
                return;
            }
            else if (z > 0.1f || z < -0.1f)
            {
                isSprinting = true;
                return;
            }
            isSprinting = false;
        }
    }

    void StopSprinting()
    {
        isSprinting = false;
    }

    void controllerInput()
    {
        int joyOffSet = offSetToNextJoy * (joyID - 1);
        x = Input.GetAxisRaw("Joy" + joyID + "ThumbStickLeftHorizontal");
        z = Input.GetAxisRaw("Joy" + joyID + "ThumbStickLeftVertical");
        if (Mathf.Abs(x) < 0.3f)
        {
            x = 0;
        }
        if (Mathf.Abs(z) < 0.3f)
        {
            z = 0;
        }
        if (haveBall)

        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button7 + joyOffSet) )
            {
                StartCharging();

            }
            if (isCharging)
            {
                if (!isThrowing)
                {
                    //Debug.Log(currentThrowForce);
                    if (currentThrowForce < throwForceMax)
                    {
                        currentThrowForce += throwChargeTime * Time.deltaTime;
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick1Button2 + joyOffSet))
                    {
                        currentThrowForce = throwForceMin;
                        StopCharging();
                    }
                    else if (Input.GetKeyUp(KeyCode.Joystick1Button7 + joyOffSet))
                    {
                        ThrowBall(currentThrowForce);
                    }
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0 + joyOffSet))
            {
                grabBall();
            }
            //Not using
            //if (Input.GetKeyDown(KeyCode.Joystick1Button2 + joyOffSet))
            //{
            //    isCatching = true;
            //    capTrigger.skill(true);
            //}
            //else if (Input.GetKeyUp(KeyCode.Joystick1Button2 + joyOffSet))
            //{
            //    isCatching = false;
            //    capTrigger.skill(false);
            //}
        }

        

       

        ////Throw Trap Input
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    if (currentTrapAmmo > 0)
        //    {
        //        GameObject trap = Instantiate(trapPrefab, hand.position + hand.forward, new Quaternion()) as GameObject;
        //        trap.GetComponent<Rigidbody>().AddForce(hand.forward * trapThrowForce, ForceMode.Impulse);
        //        currentTrapAmmo--;
        //        TrapText.GetComponent<Text>().text = currentTrapAmmo.ToString();
        //    }
        //}

        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    triggerObject.GetComponent<CaptureBallTrigger>().skillTrue();
        //}

        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            isCrouching = true;
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button3))
        {
            isCrouching = false;
        }


        //Jump
        if (Input.GetKeyDown(KeyCode.Joystick1Button1 + joyOffSet))
        {
            jumpStart = true;
        }
            //Sprinting
            if (Input.GetKeyDown(KeyCode.Joystick1Button6 + joyOffSet))
            {
                StartSprinting();
            }
            else if (Input.GetKeyUp(KeyCode.Joystick1Button6 + joyOffSet))
            {
                StopSprinting();
            }
    }
    void MyPCInput()
    {

        //Movement Input
        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");

        //Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        //rayOrigin += fpsCam.transform.forward * 1.05f;
        //Debug.DrawRay(rayOrigin, fpsCam.transform.forward * pickUpBallDis, Color.red);

        if (haveBall)

        {
            if (Input.GetMouseButtonDown(0))
            {
                StartCharging();

            }
            if (isCharging)
            {
                if (!isThrowing)
                {
                    if (currentThrowForce < throwForceMax)
                    {
                        currentThrowForce += throwChargeTime * Time.deltaTime;
                    }
                    if (Input.GetMouseButton(1))
                    {
                        currentThrowForce = throwForceMin;
                        StopCharging();
                    }
                    else if (Input.GetMouseButtonUp(0))
                    {
                        ThrowBall(currentThrowForce);
                    }
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                grabBall();
            }
            //Not Using
            //if (Input.GetKeyDown(KeyCode.Q))
            //{
            //    isCatching = true;
            //    capTrigger.skill(true);
            //}
            //else if (Input.GetKeyUp(KeyCode.Q))
            //{
            //    isCatching = false;
            //    capTrigger.skill(false);
            //}

        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            isCrouching = true;
        }
        else if (Input.GetKeyUp(KeyCode.C))
        {
            isCrouching = false;
        }

       

        ////Throw Trap Input
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    if (currentTrapAmmo > 0)
        //    {
        //        GameObject trap = Instantiate(trapPrefab, hand.position + hand.forward, new Quaternion()) as GameObject;
        //        trap.GetComponent<Rigidbody>().AddForce(hand.forward * trapThrowForce, ForceMode.Impulse);
        //        currentTrapAmmo--;
        //        TrapText.GetComponent<Text>().text = currentTrapAmmo.ToString();
        //    }
        //}

        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    triggerObject.GetComponent<CaptureBallTrigger>().skillTrue();
        //}

        //Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpStart = true;
        }

        //Sprinting
      
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                StartSprinting();
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                StopSprinting();
            }
  
    }

    void StartCharging()
    {
        isCharging = true;
        SM.PlayPlayerSoundEffect("Charging");
    }

    void StopCharging()
    {
        isCharging = false;
        SM.StopPlayingPlayerSoundEffect("Charging");
    }

    public void gotHit()
    {
        if (!isDead)
        {
            isDead = true;
            if (haveBall)
            {
                ThrowBall(0);
            }
            RagdollActive(true);
            SM.PlayPlayerSoundEffect("PlayerHit");
        }
    }

    void ThrowBall(float throwForce)
    {
        SM.PlayPlayerSoundEffect("Throw");
        isThrowing = true;
        ballColliderObj.GetComponent<Ball>().Thrown();
        ballColliderObj.enabled = true;
        ballRigidObj.constraints = RigidbodyConstraints.None;
        ballRigidObj.transform.parent = null;
        ballRigidObj.transform.position = throwPosition.position;

        ballRigidObj.AddForce(cameraTransform.forward * throwForce, ForceMode.Impulse);

        currentThrowForce = throwForceMin;
        haveBall = false;
        StopCharging();
    }
    void grabBall()
    {
        RaycastHit hit;
        bool isHit = Physics.SphereCast(cameraTransform.position + cameraTransform.forward * 1.05f, radiusPickingUpBall, cameraTransform.forward, out hit, pickUpBallDis);
        if (isHit)
        {
            if (hit.transform.gameObject.CompareTag("ballOnGround") || hit.transform.gameObject.CompareTag("Projectile"))
            {
                haveBall = true;
                SM.PlayPlayerSoundEffect("GrabBall");
                Ball ball = hit.transform.GetComponent<Ball>();
                int currentBallOwnerID = ball.getBallOwnerID();
                if (currentBallOwnerID != -1 && currentBallOwnerID != playerID)
                {
                    GameObject otherPlayer = GameObject.FindGameObjectWithTag("Player " + currentBallOwnerID);
                    if (otherPlayer)
                    {
                        PlayerProperty Pp = otherPlayer.GetComponent<PlayerProperty>();
                        if (!Pp.getIsDead())
                        {
                            Pp.ballGotCaught();
                        }
                    }
                }
                ball.setBallOwner(playerID, playerColor);
                
                ballColliderObj = hit.collider;
                ballRigidObj = hit.rigidbody;
                hit.transform.SetParent(Hand.transform);
                hit.transform.position = Hand.transform.position;
                hit.rigidbody.constraints = RigidbodyConstraints.FreezePosition;
                hit.collider.enabled = false;
                
            }
        }
    }

    //Not using
    //public void caughtBall(Rigidbody ballRigidBody, Collider ballCollider)
    //{

    //    Ball ball = ballRigidBody.transform.GetComponent<Ball>();
    //    ball.setBallOwner(playerID, playerColor);

    //    this.ballRigidObj = ballRigidBody;
    //    this.ballColliderObj = ballCollider;

    //    ballColliderObj = ballCollider;
    //    ballRigidObj = ballRigidBody;
    //    ballRigidBody.transform.SetParent(Hand.transform);
    //    ballRigidBody.transform.position = Hand.transform.position;
    //    ballRigidBody.constraints = RigidbodyConstraints.FreezePosition;
    //    ballCollider.enabled = false;
    //    haveBall = true;
    //    capTrigger.skill(false);
    //    isCatching = false;
    //    //this.ball = ball.Raycast;
    //}

    public void ballGotCaught()
    {
        gotHit();
    }

    public float getX()
    {
        return x;
    }
    public float getZ()
    {
        return z;
    }

    public bool getIsSprinting()
    {
        return isSprinting;
    }

    public float getCurrentThrowForce()
    {
        return currentThrowForce;
    }


    public bool getHaveBall()
    {
        return haveBall;
    }
    
    public int getPlayerID()
    {
        return playerID;
    }

    public int getJoyID()
    {
        return joyID;
    }

    public Color getPlayerColor()
    {
        return playerColor;
    }

    public bool getIsDead()
    {
        return isDead;
    }

    public bool getIsCrouching()
    {
        return isCrouching;
    }

    public bool getIsCatching()
    {
        return isCatching;
    }

   public float getCurrentStamina()
    {
        return currentStaminaTime;
    }

    public bool getIsCharging()
    {
        return isCharging;
    }

    public bool getIsThrowing()
    {
        return isThrowing;
    }

    public void SetIsThrowingToFalse()
    {
        isThrowing = false;
    }
    
}
