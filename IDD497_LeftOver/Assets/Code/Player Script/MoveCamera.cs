﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Transform playerHead;
    public GameObject player;
    private PlayerProperty PP;
    public GameObject CameraPrefab;
    public float cameraSmoothing = 10;
    // Start is called before the first frame update
    void Start()
    {
        if (player)
        {
            PP = player.GetComponent<PlayerProperty>();
        } 
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, playerHead.position, cameraSmoothing * Time.deltaTime);
    }
}
