﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{


    private Rigidbody RB;

    private PlayerProperty PP;

    private Transform GO;

    private CapsuleCollider RC;


    float distToGround;

    // Start is called before the first frame update
    void Start()
    {
        PP = gameObject.GetComponent<PlayerProperty>();
        RB = PP.RB;
        RC = PP.RC;
        GO = PP.body;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        //checkIfGrounded();
        if (!PP.getIsDead())
        {
            Movement();
        }
    }

    public void StartCrouch()
    {
        RC.height -= PP.normalToCrouchHeightMinus;
        RC.center -= new Vector3(0, PP.normalToCrouchHeightMinus / 2, 0);
        
    }
    public void StopCrouch()
    {
        RC.height += PP.normalToCrouchHeightMinus;
        RC.center += new Vector3(0, PP.normalToCrouchHeightMinus / 2, 0);
    }

    private void Movement()
    {
        RB.AddForce(GO.up * -10 * Time.deltaTime);
        Vector2 mag = FindRetVelLook();
        float speed = PP.walkAcc;
        float MaxSpeed = PP.walkMaxSpeed;
        bool isSprinting = PP.getIsSprinting();
        float x = PP.getX();
        float z = PP.getZ();

        if (isSprinting)
        {
            speed = PP.runAcc;
            MaxSpeed = PP.runMaxSpeed;
        }

        if (!IsGrounded())
        {
            MaxSpeed = MaxSpeed * 0.5f;
            speed = speed * 0.25f;
        }
        else if (PP.getIsCrouching())
        {
            speed = speed * 0.25f;
        }
 
        CounterMovement(x, z, mag);

        if (mag.x > MaxSpeed)
        {
            x = 0;
        }
        if (mag.y > MaxSpeed)
        {
            z = 0;
        }
        if (mag.x < -MaxSpeed)
        {
            x = 0;
        }
        if (mag.y < -MaxSpeed)
        {
            z = 0;
        }

        RB.AddForce(GO.transform.forward * z * speed * Time.deltaTime);
        RB.AddForce(GO.transform.right * x * speed * Time.deltaTime);


    }

    private void CounterMovement(float x, float y, Vector2 mag)
    {
        float thresHold = PP.thresHold;
        float walkSpeed = PP.walkAcc;
        float counterMovement = PP.counterMovement;
        if (Mathf.Abs(mag.x) > thresHold && Mathf.Abs(x) < 0.05f || (mag.x < -thresHold && x > 0) || (mag.x > thresHold && x < 0))
        {
            RB.AddForce(walkSpeed * GO.transform.right * Time.deltaTime * -mag.x * counterMovement);
        }
        if (Mathf.Abs(mag.y) > thresHold && Mathf.Abs(y) < 0.05f || (mag.y < -thresHold && y > 0) || (mag.y > thresHold && y < 0))
        {
            RB.AddForce(walkSpeed * GO.transform.forward * Time.deltaTime * -mag.y * counterMovement);
        }
    }

    public Vector2 FindRetVelLook()
    {
        float lookAngle = GO.transform.eulerAngles.y;
        float moveAngle = Mathf.Atan2(RB.velocity.x, RB.velocity.z) * Mathf.Rad2Deg;

        float u = Mathf.DeltaAngle(lookAngle, moveAngle);
        float v = 90 - u;

        float magnitude = RB.velocity.magnitude;
        float yMag = magnitude * Mathf.Cos(u * Mathf.Deg2Rad);
        float xMag = magnitude * Mathf.Cos(v * Mathf.Deg2Rad);

        return new Vector2(xMag, yMag);
    }

    public bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, RC.bounds.extents.y + PP.groundDistance);
    }

    public void StartJump()
    {
        if (IsGrounded())
        {
            PP.jumpStart = false;
            RB.AddForce(GO.transform.up * PP.jumpForce);
        }
    }

    public void landed()
    {
        PP.landed = true;
        PP.isFalling = false;
    }


}
