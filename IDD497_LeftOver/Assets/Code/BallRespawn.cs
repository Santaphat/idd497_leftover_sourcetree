﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRespawn : MonoBehaviour
{
    bool canEnter;
    public float ballRespawnTime = 40.0f;

    float ballRespawnTimerCountDown;
    // Start is called before the first frame update
    void Start()
    {
        canEnter = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!canEnter)
        {
            ballRespawnTimerCountDown -= Time.deltaTime;
            if(ballRespawnTimerCountDown < 0)
            {
                canEnter = true;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (canEnter)
        {
            if (col.tag == "Player")
            {
                RB_PlayerController player =  col.GetComponent<RB_PlayerController>();
                if (player.AddAmmo())
                {
                    Debug.Log("Enter");
                    canEnter = false;
                    ballRespawnTimerCountDown = ballRespawnTime;
                }
            }
        }
    }
}
