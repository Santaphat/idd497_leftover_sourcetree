﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTest : MonoBehaviour
{
 
    Rigidbody test;
    Collider test1;
    public GameObject trigger;
    bool hitGround;
    GameObject TriggerObject;
    // Start is called before the first frame update
    void Start()
    {
        test = GetComponent<Rigidbody>();
        test1 = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (hitGround)
        {
            if(TriggerObject == null)
            {
                Destroy(gameObject);
            }
        }
       
    }

    void OnCollisionEnter(Collision col)
    {
        if (!hitGround)
        {
            if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
               //Debug.Log(gameObject.name + "Was collided with" + col.gameObject.name);
                test.isKinematic = true;
                hitGround = true;
                TriggerObject = Instantiate(trigger, gameObject.transform.position, new Quaternion()) as GameObject;
            }
        }
    }
}
