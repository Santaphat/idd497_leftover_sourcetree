﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTrigger : MonoBehaviour
{

    bool triggered;
    TestEnemy triggeredObject;
    public float enemyCoolDown = 10.0f;
    float enemyCoolDownTimer;

    public float trapActiveTime = 60.0f;
    float trapActiveTimer;

    // Start is called before the first frame update
    void Start()
    {
        trapActiveTimer = trapActiveTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (triggered)
        {
            //Debug.Log(enemyCoolDownTimer);
            if (enemyCoolDownTimer > 0) {
                enemyCoolDownTimer -= Time.deltaTime;
            }
            else
            {
                triggeredObject.setIsTrapped(false);
                Destroy(gameObject);
            }
        }
        else
        {
            if(trapActiveTimer < 0)
            {
                Destroy(gameObject);
            }
            trapActiveTimer -= Time.deltaTime;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!triggered)
        {
            Debug.Log(other.name + "Hit");
            if (other.tag == "enemy")
            {
                triggered = true;
                enemyCoolDownTimer = enemyCoolDown;
      
                triggeredObject = other.GetComponent<TestEnemy>();
                triggeredObject.setIsTrapped(true);
            }
        }
    }
}
