﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public GameObject BGMusic;

    // Start is called before the first frame update
    void Start()
    {
        if(BGMusic != null)
        {
            Destroy(BGMusic);
            Instantiate<GameObject>(BGMusic);
        }
    }
}
