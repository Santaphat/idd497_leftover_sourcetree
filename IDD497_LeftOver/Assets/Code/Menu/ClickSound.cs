﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ClickSound : MonoBehaviour
{
    public AudioClip sound;
    public string nextScene;

    public bool stopMusic = false;
    
    private Button button { get { return GetComponent<Button>(); } }
    private AudioSource source { get { return GetComponent<AudioSource>(); } }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;

        button.onClick.AddListener(() => PlaySound());
    }

    private IEnumerator WaitAndLoadScene(float clipLength, string sceneName)
    {
        yield return new WaitForSeconds(clipLength);
        SceneManager.LoadScene(sceneName);
    }

    private IEnumerator WaitAndExit(float clipLength)
    {
        Debug.Log("Exit the game.");
        yield return new WaitForSeconds(clipLength);        
        Application.Quit();
    }

    private IEnumerator WaitToFinishSound(float clipLength)
    {
        yield return new WaitForSeconds(clipLength);
    }

    void PlaySound()
    {
        source.PlayOneShot(sound);
        if (nextScene == "Exit")
        {
            Debug.Log("Bye!!");
            StartCoroutine(WaitAndExit(sound.length));
            stopTheMusic(stopMusic);
        }
        else if (nextScene == "None")
        {
            Debug.Log("Not changing scene, just play the sound.");
            StartCoroutine(WaitToFinishSound(sound.length));
            stopTheMusic(stopMusic);
        }
        else
        {
            Debug.Log("Go to [" + nextScene.ToString() + "] scene.");
            StartCoroutine(WaitAndLoadScene(sound.length, nextScene));
            stopTheMusic(stopMusic);
        }
    }

    void stopTheMusic(bool stop)
    {
        if(stop == true)
        {
            GameObject.FindGameObjectWithTag("Music").GetComponent<MainMenuBGMusic>().StopMusic();
        }
        else
        {
            GameObject.FindGameObjectWithTag("Music").GetComponent<MainMenuBGMusic>().PlayMusic();
        }
    }
}
