﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameObject ball;
    public float coolDownTimerMax = 1;
    private SoundManager SM;

    private float coolDownTimer;

    private Collider CO;
    private Renderer RD;

    private Light lightVar;
    private ParticleSystem PS;
    ParticleSystem.MainModule ma;

    int currentOwnerID = 1;

    bool playerPickUpBall = false;
    bool playerHoldingBall = false;


    Color currentPlayerColor = new Color(1, 0, 0);
    Color canTake = new Color32(205, 97, 51, 255);

    public float groundDistance = 0.1f;

    void Start()
    {
        coolDownTimer = 0;
        CO = gameObject.GetComponent<Collider>();
        RD = gameObject.GetComponent<Renderer>();
        SM = FindObjectOfType<SoundManager>();

        lightVar = gameObject.GetComponent<Light>();
        PS = gameObject.GetComponent<ParticleSystem>();
        ma = PS.main;

    }
    // Update is called once per frame
    void Update()
    {
        if (IsGrounded())
        {
            coolDownTimer += Time.deltaTime;
            if (coolDownTimer > coolDownTimerMax)
            {
                currentOwnerID = -1;
                gameObject.tag = "ballOnGround";
                RD.material.SetColor("_EmissionColor", canTake);
                RD.material.color = canTake;
                lightVar.color = canTake;
                ma.startColor = canTake;
                RD.material.color = canTake;
            }
        }
        else
        {
            gameObject.tag = "Projectile";
            if (playerPickUpBall)
            {
                playerPickUpBall = false;
                RD.material.color = currentPlayerColor;
                lightVar.color = currentPlayerColor;
                ma.startColor = currentPlayerColor;
                RD.material.SetColor("_EmissionColor", currentPlayerColor);

            }
            coolDownTimer = 0;
        }

    }
    private bool IsGrounded()
    {
        if (!playerHoldingBall)
        {
            return Physics.Raycast(transform.position, -Vector3.up, CO.bounds.extents.y + groundDistance);
        }
        return false;
    }

    public void Thrown()
    {
        playerHoldingBall = false;
    }

    public void setBallOwner(int playerID, Color playerColor)
    {
        
        SoundManager Test = FindObjectOfType<SoundManager>();
        Test.PlaySoundEffect("Test", gameObject.transform.position);
        playerPickUpBall = true;
        playerHoldingBall = true;
        currentPlayerColor = playerColor;
        currentOwnerID = playerID;
    }
    public int getBallOwnerID()
    {
        return currentOwnerID;
    }
    void OnCollisionEnter(Collision col)
    {
        if (currentOwnerID != -1)
        {
            if (col.gameObject.tag.Contains("Player"))
            {
                if (col.gameObject.tag != "Player " + currentOwnerID)
                {
                    PlayerProperty PP = col.gameObject.GetComponent<PlayerProperty>();
                    PP.gotHit();
                }
            }
            else
            {
                SM.PlaySoundEffect("BallCollision", gameObject.transform.position);
            }
        }
        else
        {
            SM.PlaySoundEffect("BallCollision", gameObject.transform.position);
        }
        //if (col.gameObject.tag == "enemy")
        //{

        //    //Debug.Log("Hit");
        //    col.gameObject.GetComponent<TestEnemy>().setIsHit(true);
        //    Destroy(ball);
        //}

    }
}
