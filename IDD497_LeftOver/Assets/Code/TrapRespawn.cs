﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapRespawn : MonoBehaviour
{
    bool canEnter;
    public float trapRespawnTime = 40.0f;

    float trapRespawnTimerCountDown;
    // Start is called before the first frame update
    void Start()
    {
        canEnter = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!canEnter)
        {
            trapRespawnTimerCountDown -= Time.deltaTime;
            if (trapRespawnTimerCountDown < 0)
            {
                canEnter = true;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (canEnter)
        {
            if (col.tag == "Player")
            {
                RB_PlayerController player = col.GetComponent<RB_PlayerController>();
                if (player.AddTrapAmmo())
                {
                    Debug.Log("Enter");
                    canEnter = false;
                    trapRespawnTimerCountDown = trapRespawnTime;
                }
            }
        }
    }
}
