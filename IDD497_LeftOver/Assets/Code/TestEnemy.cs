﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEnemy : MonoBehaviour
{
    int ghostLayer;
    int normalLayer;

    bool isTrapped = false;
    bool isHit = false;
    public Transform[] path;

    public float walkSpeed = 1000.0f;

    public int currentPath = 0;
    public float disAroundPath = 3.0f;

    Rigidbody RB;

    public bool canTrap = false;
    public bool canHit = false;

    public bool pathWalkOn = false;
    bool canWalk = true;

    float isHitCoolDown;
    public float HitCoolDownTimer = 4.0f;

    Renderer test;

    Color inTrap;
    Color Normal;
    Color inHit;

    // Start is called before the first frame update
    void Start()
    {
        isHitCoolDown = HitCoolDownTimer;
        inTrap = new Color(0, 0, 1);
        Normal = new Color(1, 1, 1);
        inHit = new Color(1, 0, 0, 0.1f);
        ghostLayer = LayerMask.NameToLayer("Ghost");
        normalLayer = LayerMask.NameToLayer("Enemy");
        RB = GetComponent<Rigidbody>();
        test = gameObject.GetComponent<Renderer>();
      
    }

    public void setIsTrapped(bool input)
    {
        isTrapped = input;
    }

    public void setIsHit(bool input)
    {
        isHit = input;
    }
    // Update is called once per frame
    void Update()
    {
        if (pathWalkOn)
        {
            if (canTrap)
            {
                if (isTrapped && !isHit)
                {
                    canWalk = false;
                    test.material.color = inTrap;
                }
                else
                {
                    canWalk = true;
                 
                }
            }
        }
        if (canHit)
        {
            if (isHit)
            {
                test.material.color = inHit;
                gameObject.layer = ghostLayer;
                isHitCoolDown -= Time.deltaTime;
                if(isHitCoolDown < 0)
                {
                    isHit = false;
                    isHitCoolDown = HitCoolDownTimer;
                }
            }
           
        }
        if (!isHit)
        {
            gameObject.layer = normalLayer;
        }
        if(!isTrapped && !isHit)
        {
            test.material.color = Normal;
        }
    }
    void FixedUpdate()
    {

        if (pathWalkOn)
        {
            if (canWalk)
            {
                walkOnPath();
            }
            else
            {
                RB.velocity = new Vector3(0, 0, 0);
            }
        }
        
    }

    void walkOnPath()
    {
        Vector3 dis = path[currentPath].position - transform.position;
        dis.y = 0;
        float test = dis.magnitude;
        if (test < disAroundPath) {
            chooseNextPath();
        }
        else
        {
            RB.AddForce(dis.normalized * walkSpeed * Time.deltaTime);
        }

       

    }

    void chooseNextPath()
    {
        if(currentPath >= path.Length - 1)
        {
            currentPath = 0;
        }
        else
        {
            currentPath++;
        }
    }
 
}
