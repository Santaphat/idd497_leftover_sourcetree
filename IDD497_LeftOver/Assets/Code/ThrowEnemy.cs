﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowEnemy : MonoBehaviour
{
    public GameObject ballPrefab;
    public float throwForce = 100.0f;

    public float coolDownTimer = 5.0f;
    float timer;

    bool canThrow = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (canThrow)
        {
            GameObject ball = Instantiate(ballPrefab, transform.position + transform.forward, transform.rotation) as GameObject;
            ball.GetComponent<Rigidbody>().AddForce(transform.forward * throwForce, ForceMode.Impulse);
            ball.GetComponent<Ball>().setBallOwner(0, new Color(1, 0, 0));
            timer = coolDownTimer;
            canThrow = false;
        }
        else
        {
            timer -= Time.deltaTime;
            if(timer < 0)
            {
                canThrow = true;
            }
        }
    }
}
