﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour
{
    public GameObject winPanel;
    public GameMode gameMode;

    public Text winText;

    private bool winDeclared = false;

    // Start is called before the first frame update
    void Start()
    {
        winPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!winDeclared)
        {
            if (gameMode.GetAlivePlayers().Count == 1)
            {
                winPanel.SetActive(true);

                int winPlayerID = gameMode.PlayerIDWon();

                winText.text = "Player " + winPlayerID + " Won!";
                winText.color = GameStaticVariable.GetPlayer()[winPlayerID-1].getPlayerColor();
                //for (int i = 0; i < GameStaticVariable.GetPlayer().Count; i++)
                //{
                //    Debug.Log("i:" + i + " id:" + GameStaticVariable.GetPlayer()[i].getPlayerID());
                //}
                Cursor.lockState = CursorLockMode.None;

                winDeclared = true;
            }
        }
        
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
