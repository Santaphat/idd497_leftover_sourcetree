﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    
    public string name;

    public AudioClip clip;

    [Range(0.0f, 1.0f)]
    public float volume;

    [Range(0.1f, 3.0f)]
    public float pitch;

    [HideInInspector]
    public AudioSource source;

    public bool loop;
    public float listingDistance;
}
public class SoundManager : MonoBehaviour
{

    GameMode GM;
    public Sound[] sounds;

    private void Awake()
    {
        GM = gameObject.GetComponent<GameMode>();
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //PlayMusic("InGameMusic");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySoundEffect(string name, Vector3 SourcePosition)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        if (s == null)
            return;
        float minDist = float.MaxValue;
        foreach (playerObject p in GM.GetAlivePlayers())
        {

            float dist = Vector3.Distance(SourcePosition, p.GetPlayerProperty().transform.position);
            if (minDist > dist)
            {
                minDist = dist;
            }
        }
        if (minDist < s.listingDistance)
        {
            float d = 1 -(minDist / s.listingDistance);
            //Debug.Log(d);
            //Debug.Log(s.source.volume);
            //s.source.Play();
            AudioSource.PlayClipAtPoint(s.source.clip, gameObject.transform.position , s.source.volume * d);
            s.source.volume = s.volume;
        }
    }

    public void PlayPlayerSoundEffect(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        if (s == null)
            return;
        s.source.Play();
    }

    public void StopPlayingPlayerSoundEffect(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        if (s == null)
            return;
        s.source.Stop();
    }
    public void PlayMusic(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        if (s == null)
            return;
        s.source.Play();
    }

    public void PauseMusic(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        if (s == null)
            return;
        s.source.Pause();
    }
}
