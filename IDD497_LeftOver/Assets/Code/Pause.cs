﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public static bool isPaused = false;

    public GameObject pausePanel;

    private bool RestrictMenu = false;
    private int pauserPlayerID = -1;
    private int joyOffset = KeyCode.Joystick2Button0 - KeyCode.Joystick1Button0;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            
            if (isPaused)
            {

                if(!RestrictMenu || pauserPlayerID.Equals(1))
                {
                    ResumeGame();
                }
                
            }
            else
            {
                PauseGame(1);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button9 + i * joyOffset))
            {
                if (isPaused)
                {
                    if (!RestrictMenu || pauserPlayerID.Equals(i + 2))
                    {
                        ResumeGame();
                    }

                }
                else
                {
                    PauseGame(i + 2);
                }
            }
        }

        //for (int i = 0; i < 3; i++)
        //{
        //    if (Input.GetButtonDown("Pause"))
        //    {
        //        if (isPaused)
        //        {
        //            if (pauserPlayerID.Equals(i + 2))
        //            {
        //                ResumeGame();
        //            }

        //        }
        //        else
        //        {
        //            PauseGame(i + 2);
        //        }
        //        Debug.Log("Pressed");
        //    }
        //}
    }

    public void PauseGame(int playerID)
    {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);

        //PlayerProperty[] ppInstances = FindObjectsOfType<PlayerProperty>();
        //foreach (PlayerProperty instance in ppInstances)
        //{
        //    //Debug.Log(instance.gameObject.name);
        //    instance.enabled = false;
        //}

        Cursor.lockState = CursorLockMode.None;

        pausePanel.transform.GetChild(0).GetComponent<Text>().text = "Player " + playerID + " has paused";

        isPaused = true;
        pauserPlayerID = playerID;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);

        //PlayerProperty[] ppInstances = FindObjectsOfType<PlayerProperty>();
        //foreach (PlayerProperty instance in ppInstances)
        //{
        //    //Debug.Log(instance.gameObject.name);
        //    instance.enabled = true;
        //}

        Cursor.lockState = CursorLockMode.Locked;

        isPaused = false;
        pauserPlayerID = -1;
    }
}
