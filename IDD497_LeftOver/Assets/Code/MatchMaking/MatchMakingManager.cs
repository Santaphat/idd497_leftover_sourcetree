﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MatchMakingManager : MonoBehaviour
{
    //Level selection
    public string[] listOfLevel;
    public Sprite[] gallary;
    public Image displayImage;

    public GameObject nextButton;
    public GameObject previousButton;

    public int imgCount;

    //Player Display
    public Transform playerContainer;
    public GameObject playerListPrefabs;
    private List<playerVariable> playerListing;
    private int playerCount;

    public GameObject startButton;

    //playerInfo
    private bool keyboardJoin = false;
    private bool joy1Join = false;
    private bool joy2Join = false;
    private bool joy3Join = false;

    private int kbID = 1;
    private int j1ID = 2;
    private int j2ID = 3;
    private int j3ID = 4;

    //Color
    Color32 red = new Color32(192, 57, 43, 255);
    Color32 blue = new Color32(41, 128, 185, 255);
    Color32 yellow = new Color32(241, 196, 15, 255);
    Color32 green = new Color32(39, 174, 96, 255);
    Color32 black = new Color32(44, 62, 80, 255);


    private void Awake()
    {
        startButton.SetActive(false);
        playerListing = new List<playerVariable>();
        playerCount = 0;
        imgCount = 0;
    }

    private void Update()
    {
        ListPlayers();
        CanGameStart();
        displayImage.sprite = gallary[imgCount];
    }

    //Check if there is 2 or more player to start the game
    void CanGameStart()
    {
        if(playerCount > 1)
        {
            startButton.SetActive(true);
        }
    }

    //Set the player who is player 1,2,... and add it to playerVariable
    void SetPlayer(int whoJoin)
    {
        playerCount++;
        GameObject tempListing = Instantiate(playerListPrefabs, playerContainer);
        PlayerPanel tempPanel = tempListing.GetComponent<PlayerPanel>();

        if(whoJoin == 1)
        {
            tempPanel.SetPlayerPanel(playerCount, "Keyboard", GetPlayerColor(playerCount));
            playerListing.Add(new playerVariable(GetPlayerColor(playerCount), playerCount, 0));
            
        }
        else if(whoJoin == 2)
        {
            tempPanel.SetPlayerPanel(playerCount, "Joy 1", GetPlayerColor(playerCount));
            playerListing.Add(new playerVariable(GetPlayerColor(playerCount), playerCount, 1));
        }
        else if(whoJoin == 3)
        {
            tempPanel.SetPlayerPanel(playerCount, "Joy 2", GetPlayerColor(playerCount));
            playerListing.Add(new playerVariable(GetPlayerColor(playerCount), playerCount, 2));
        }
        else if (whoJoin == 4)
        {
            tempPanel.SetPlayerPanel(playerCount, "Joy 3", GetPlayerColor(playerCount));
            playerListing.Add(new playerVariable(GetPlayerColor(playerCount), playerCount, 3));
        }
    }

    private Color32 GetPlayerColor(int playerCount)
    {
        if (playerCount == 1)
        {
            return red;
        }
        else if (playerCount == 2)
        {
            return blue;
        }
        else if (playerCount == 3)
        {
            return yellow;
        }
        else if (playerCount == 4)
        {
            return green;
        }
        return black;
    }

    //make all the player see the player panel
    void ListPlayers()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(keyboardJoin == false)
            {
                Debug.Log("Keyboard user join the game.");
                keyboardJoin = true;
                SetPlayer(kbID);
            }
            else
            {
                Debug.Log("Keyboard already join!!");
            }
            
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button0))//press SQUARE to join
        {
            if(joy1Join == false)
            {
                Debug.Log("Joy1 user join the game.");
                joy1Join = true;
                SetPlayer(j1ID);
            }
            else
            {
                Debug.Log("Joy1 already join!!");
            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick2Button0))
        {
            if(joy2Join == false)
            {
                Debug.Log("Joy2 user join the game.");
                joy2Join = true;
                SetPlayer(j2ID);
            }
            else
            {
                Debug.Log("Joy2 already join!!");
            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick3Button0))
        {
            if (joy3Join == false)
            {
                Debug.Log("Joy3 user join the game.");                
                joy3Join = true;
                SetPlayer(j3ID);
            }
            else
            {
                Debug.Log("Joy3 already join!!");
            }
        }
    }

    //Set player to static when start
    public void StartGame()
    {
        GameStaticVariable.setPlayer(playerListing);
        foreach(playerVariable pv in playerListing)
        {
            Debug.Log("JoyID: " + pv.getJoyID() + " PlayerID: " + pv.getPlayerID() + " PlayerColor: " + pv.getPlayerColor().ToString());
        }
        SceneManager.LoadScene(listOfLevel[imgCount]);
        Debug.Log("Map " + listOfLevel[imgCount] + "is selected");
    }

    public void BtnNext()
    {
        if(imgCount + 1 < gallary.Length)
        {
            imgCount++;
        }
    }

    public void BtnPrev()
    {
        if (imgCount - 1 >= 0)
        {
            imgCount--;
        }
    }
}
