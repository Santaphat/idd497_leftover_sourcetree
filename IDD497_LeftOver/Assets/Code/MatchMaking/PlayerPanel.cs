﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour
{
    public Text playerNum;
    public Text playerStat;
    public Image playerColor;

    public void SetPlayerPanel(int num, string type, Color32 color)
    {
        playerNum.text = "Player " + num.ToString();
        playerStat.text = "Connected " + type;
        playerColor.GetComponent<Image>().color = color;
    }
}
