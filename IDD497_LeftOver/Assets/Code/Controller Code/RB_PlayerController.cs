﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RB_PlayerController : MonoBehaviour
{
    Rigidbody RB;
    private float x, z;
    private bool isSprinting;

    public Transform Head;

    public float groundDistance = 0.5f;
    public float characterSize = 2.0f;

    bool isGrounded = false;

    public float walkSpeed = 4500.0f;
    public float walkMaxSpeed = 15.0f;

    public float runSpeed = 5500.0f;
    public float runMaxSpeed = 20.0f;

    public float jumpForce = 550.0f;

    public float thresHold = 0.01f;
    public float counterMovement = 0.175f;

    //UI
    public Transform ammoText;
    public Transform TrapText;

    //Throwing

    public int maxBallAmmo = 3;
    public int MaxTrapAmmo = 2;

    public GameObject ballPrefab;

    public float throwForceMax = 100.0f;
    public float throwForceMin = 20.0f;
    public float trapThrowForce = 20.0f;

    public GameObject trapPrefab;

    public Transform hand;

    public float currentThrowForce;

    int currentBallAmmo = 0;
    int currentTrapAmmo = 0;

    public GameObject triggerObject;

    // Start is called before the first frame update
    void Start()
    {
        currentBallAmmo = maxBallAmmo;
        currentTrapAmmo = MaxTrapAmmo;
        ammoText.GetComponent<Text>().text = currentBallAmmo.ToString();
        TrapText.GetComponent<Text>().text = currentTrapAmmo.ToString();
        currentThrowForce = throwForceMin;
        RB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        MyInput();
    }
    void MyInput()
    {
        //Movement Input
        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");
        if (Input.GetMouseButton(0))
        {
            if (currentThrowForce < throwForceMax)
            {
                currentThrowForce += 50 * Time.deltaTime;
            }
        }

        //Throw Ball Input
        if (Input.GetMouseButtonUp(0))
        {
            if (currentBallAmmo > 0)
            {
                //Vector2 objVel = FindRetVelLook();
                GameObject ball = Instantiate(ballPrefab, hand.position + hand.forward, hand.rotation) as GameObject;
                ball.GetComponent<Rigidbody>().AddForce(hand.forward * currentThrowForce, ForceMode.Impulse);
                currentThrowForce = throwForceMin;
                currentBallAmmo--;
                ammoText.GetComponent<Text>().text = currentBallAmmo.ToString();
                
            }
        }

        //Throw Trap Input
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (currentTrapAmmo > 0)
            {
                GameObject trap = Instantiate(trapPrefab, hand.position + hand.forward, new Quaternion()) as GameObject;
                trap.GetComponent<Rigidbody>().AddForce(hand.forward * trapThrowForce, ForceMode.Impulse);
                currentTrapAmmo--;
                TrapText.GetComponent<Text>().text = currentTrapAmmo.ToString();
            }
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            
        }

        //Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
            {
                jump();
            }
        }

        //Sprinting
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isSprinting = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isSprinting = false;
        }



    }

    private void checkIfGrounded()
    {
        Vector3 position = transform.position;
        position.y -= characterSize;

        if (Physics.Raycast(position, -Head.up, groundDistance))
        {
            //Debug.Log("True");
            isGrounded = true;
        }
        else
        {
            //Debug.Log("False");
            isGrounded = false;
        }
    }

    private void jump()
    {
        RB.AddForce(Head.transform.up * jumpForce);
    }
    private void FixedUpdate()
    {
        checkIfGrounded();
        Movement();
    }

    private void Movement()
    {
        Vector2 mag = FindRetVelLook();
        float MaxSpeed = walkMaxSpeed;
        if (isSprinting == true)
        {
            MaxSpeed = runMaxSpeed;
        }
        CounterMovement(x, z, mag);
        if (mag.x > MaxSpeed)
        {
            x = 0;
        }
        if (mag.y > MaxSpeed)
        {
            z = 0;
        }
        if (mag.x < -MaxSpeed)
        {
            x = 0;
        }
        if (mag.y < -MaxSpeed)
        {
            z = 0;
        }
        RB.AddForce(Head.transform.forward * z * walkSpeed * Time.deltaTime);
        RB.AddForce(Head.transform.right * x * walkSpeed * Time.deltaTime);
    }

    private void CounterMovement(float x, float y, Vector2 mag)
    {
        if (Mathf.Abs(mag.x) > thresHold && Mathf.Abs(x) < 0.05f || (mag.x < -thresHold && x > 0) || (mag.x > thresHold && x < 0))
        {
            RB.AddForce(walkSpeed * Head.transform.right * Time.deltaTime * -mag.x * counterMovement);
        }
        if (Mathf.Abs(mag.y) > thresHold && Mathf.Abs(y) < 0.05f || (mag.y < -thresHold && y > 0) || (mag.y > thresHold && y < 0))
        {
            RB.AddForce(walkSpeed * Head.transform.forward * Time.deltaTime * -mag.y * counterMovement);
        }
    }

    public Vector2 FindRetVelLook()
    {
        float lookAngle = Head.transform.eulerAngles.y;
        float moveAngle = Mathf.Atan2(RB.velocity.x, RB.velocity.z) * Mathf.Rad2Deg;

        float u = Mathf.DeltaAngle(lookAngle, moveAngle);
        float v = 90 - u;

        float magnitude = RB.velocity.magnitude;
        float yMag = magnitude * Mathf.Cos(u * Mathf.Deg2Rad);
        float xMag = magnitude * Mathf.Cos(v * Mathf.Deg2Rad);

        return new Vector2(xMag, yMag);
    }

    public bool AddAmmo()
    {
        if (currentBallAmmo < maxBallAmmo)
        {
            currentBallAmmo++;
            ammoText.GetComponent<Text>().text = currentBallAmmo.ToString();
            return true;

        }
        return false;
    }

    public bool AddTrapAmmo()
    {
        if(currentTrapAmmo < MaxTrapAmmo)
        {
            currentTrapAmmo++;
            TrapText.GetComponent<Text>().text = currentTrapAmmo.ToString();
            return true;

        }
        return false;
    }
}
