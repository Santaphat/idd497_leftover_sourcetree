﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureBallTrigger : MonoBehaviour
{
    bool skillOn = false;
    public string projectileTag = "Projectile";
    public GameObject parentObject;
    // Start is called before the first frame update
    void Start()
    {
        skillOn = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (skillOn)
        {
            if (col.transform.gameObject.tag == projectileTag)
            {
                
            }
        }
    }

    public void skill(bool skill)
    {
        
        skillOn = skill;
    }
}
