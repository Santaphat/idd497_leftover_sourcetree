﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DisplayUI : MonoBehaviour
{
    PlayerProperty pp;
    private float throwForceMax;
    private float throwForceMin;
    private Color playerColor;

    public RectTransform powerShotBar;
    public RectTransform staminaBar;
    public Image border;
    public Text staminaValue;
    public Text chargeValue;
    public Text staminaText;
    public Text chargeText;

    // Start is called before the first frame update
    void Start()
    {
        pp = transform.parent.GetComponentInChildren<PlayerProperty>();
        
        throwForceMax = pp.throwForceMax;
        throwForceMin = pp.throwForceMin;

        playerColor = pp.playerColor;

        powerShotBar.GetComponent<Image>().color = playerColor;
        powerShotBar.GetChild(0).GetComponent<Image>().color = playerColor;

        //Debug.Log(powerShotBar.GetComponent<Image>().color);
        //Debug.Log(powerShotBar.GetChild(0).GetComponent<Image>().color);

        staminaBar.GetComponent<Image>().color = playerColor * 0.5f;
        staminaBar.GetChild(0).GetComponent<Image>().color = playerColor * 0.5f;

        //Debug.Log(staminaBar.GetComponent<Image>().color);
        //Debug.Log(staminaBar.GetChild(0).GetComponent<Image>().color);

        staminaText.color = playerColor;
        chargeText.color = playerColor;
        staminaValue.color = playerColor;
        chargeValue.color = playerColor;
        border.color = playerColor;
    }

    // Update is called once per frame
    void Update()
    {
        displayBars();
        displayValues();
    }

    void displayBars()
    {
        //float tPowerShot = Mathf.InverseLerp(throwForceMin, throwForceMax, pp.getCurrentThrowForce());
        //RectTransform updateBar = powerShotBar.GetChild(0).GetComponent<RectTransform>();
        //updateBar.localScale = new Vector3(updateBar.localScale.x, tPowerShot, updateBar.localScale.z);

        //float tStamina = Mathf.InverseLerp(0, pp.runStaminaTime, pp.getCurrentStamina());
        //updateBar = staminaBar.GetChild(0).GetComponent<RectTransform>();
        //updateBar.localScale = new Vector3(updateBar.localScale.x, tStamina, updateBar.localScale.z);

        RectTransform updateBar;

        updateBar = powerShotBar.GetChild(0).GetComponent<RectTransform>();
        scaleBarX(updateBar, throwForceMin, throwForceMax, pp.getCurrentThrowForce());

        updateBar = staminaBar.GetChild(0).GetComponent<RectTransform>();
        scaleBarX(updateBar, 0, pp.runStaminaTime, pp.getCurrentStamina());

    }

    void scaleBarX(RectTransform rect, float min, float max, float current)
    {
        float t = Mathf.InverseLerp(min, max, current);
        rect.localScale = new Vector3(t, rect.localScale.y, rect.localScale.z);
    }

    void scaleBarY(RectTransform rect, float min, float max, float current)
    {
        float t = Mathf.InverseLerp(min, max, current);
        rect.localScale = new Vector3(rect.localScale.x, t, rect.localScale.z);
    }

    void displayValues()
    {
        staminaValue.text = Mathf.CeilToInt((Mathf.InverseLerp(0, pp.runStaminaTime, pp.getCurrentStamina()) * 100)).ToString() + '%';
        chargeValue.text = Mathf.CeilToInt((Mathf.InverseLerp(throwForceMin, throwForceMax, pp.getCurrentThrowForce()) * 100)).ToString() + '%';
    }
}
